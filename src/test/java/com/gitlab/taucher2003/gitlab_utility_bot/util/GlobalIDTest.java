package com.gitlab.taucher2003.gitlab_utility_bot.util;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GlobalIDTest {

    @Test
    void testToString() {
        var testObjects = List.of(
            new GlobalID("Todo", "1"),
            new GlobalID("Issue", "12"),
            new GlobalID("MergeRequest", "15"),
            new GlobalID("DiffDiscussion", "b2ed2635e2f5a3ed287ee8232d913d9cc16847b6")
        );

        for (var gid : testObjects) {
            assertEquals("gid://gitlab/" + gid.type() + "/" + gid.id(), gid.toString());
        }
    }

    @Test
    void parse() {
        record TestObject(String gid, String type, String id) {}

        var testObjects = List.of(
            new TestObject("gid://gitlab/Todo/147669040", "Todo", "147669040"),
            new TestObject("gid://gitlab/Issue/95820805", "Issue", "95820805"),
            new TestObject("gid://gitlab/MergeRequest/122004375", "MergeRequest", "122004375"),
            new TestObject("gid://gitlab/DiffDiscussion/b2ed2635e2f5a3ed287ee8232d913d9cc16847b6", "DiffDiscussion", "b2ed2635e2f5a3ed287ee8232d913d9cc16847b6")
        );

        for (var testObject : testObjects) {
            var globalId = GlobalID.of(testObject.gid());
            assertEquals(testObject.type(), globalId.type());
            assertEquals(testObject.id(), globalId.id());
        }
    }
}
