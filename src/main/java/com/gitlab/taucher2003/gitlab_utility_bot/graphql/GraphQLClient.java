package com.gitlab.taucher2003.gitlab_utility_bot.graphql;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.taucher2003.gitlab_utility_bot.util.ResourceUtils;
import io.micronaut.context.annotation.Primary;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Header;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.annotation.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@Client("${gitlab.url}")
@Header(name = "Private-Token", value = "${gitlab.token}")
@Primary
public abstract class GraphQLClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(GraphQLClient.class);

    private final ObjectMapper objectMapper;

    protected GraphQLClient(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public <T> T executeQuery(String query, Class<T> type) {
        return executeQuery(query, null, type);
    }

    public <T> T executeQuery(String query, Map<String, ?> variables, Class<T> type) {
        var response = execute(query, variables, type);

        if (response.errors() != null && !response.errors().isEmpty()) {
            LOGGER.error("Encountered errors while executing query '{}': {}", query, response.errors());
        }

        return response.data();
    }

    public <T> GraphQLResponse<T> execute(String query, Map<String, ?> variables, Class<T> type) {
        var typeInformation = objectMapper.getTypeFactory().constructParametricType(GraphQLResponse.class, type);
        var response = this.executeQueryInternal(new GraphQLBody(query, variables));
        try {
            return objectMapper.readValue(response, typeInformation);
        } catch (JsonProcessingException e) {
            LOGGER.error("Failed to parse json from response", e);
            throw new RuntimeException(e);
        }
    }

    public String loadQuery(String name, String... fragments) {
        var query = new StringBuilder(ResourceUtils.readResourceByName("graphql/" + name + ".graphql"));
        for (var fragment : fragments) {
            query.append("\n").append(ResourceUtils.readResourceByName("graphql/" + fragment + ".graphql"));
        }
        return query.toString();
    }

    @Post(value = "/api/graphql", processes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    abstract String executeQueryInternal(@Body GraphQLBody body);
}
