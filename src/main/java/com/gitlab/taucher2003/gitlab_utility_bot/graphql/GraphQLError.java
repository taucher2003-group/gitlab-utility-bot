package com.gitlab.taucher2003.gitlab_utility_bot.graphql;

import io.micronaut.core.annotation.Introspected;

import java.util.List;

@Introspected
public record GraphQLError(
    String message,
    List<Location> locations,
    List<String> path,
    Extension extensions
) {

    @Introspected
    public record Location(int line, int column) {}

    @Introspected
    public record Extension(String code, String typeName) {}

    @Override
    public String toString() {
        return message;
    }
}
