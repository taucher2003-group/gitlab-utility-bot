package com.gitlab.taucher2003.gitlab_utility_bot.graphql;

import io.micronaut.core.annotation.Introspected;

import java.util.Map;

@Introspected
public record GraphQLBody(
    String query,
    Map<String, ?> variables
) {
}
