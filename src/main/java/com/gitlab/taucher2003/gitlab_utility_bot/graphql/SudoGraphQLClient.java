package com.gitlab.taucher2003.gitlab_utility_bot.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.micronaut.context.annotation.Requires;
import io.micronaut.http.annotation.Header;
import io.micronaut.http.client.annotation.Client;

@Client("${gitlab.url}")
@Header(name = "Private-Token", value = "${gitlab.sudotoken}")
@Requires(property = "gitlab.sudotoken")
public abstract class SudoGraphQLClient extends GraphQLClient {
    protected SudoGraphQLClient(ObjectMapper objectMapper) {
        super(objectMapper);
    }
}
