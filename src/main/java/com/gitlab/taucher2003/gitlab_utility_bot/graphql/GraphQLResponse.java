package com.gitlab.taucher2003.gitlab_utility_bot.graphql;

import io.micronaut.core.annotation.Introspected;

import java.util.List;

@Introspected
public record GraphQLResponse<T>(T data, List<GraphQLError> errors) {
}
