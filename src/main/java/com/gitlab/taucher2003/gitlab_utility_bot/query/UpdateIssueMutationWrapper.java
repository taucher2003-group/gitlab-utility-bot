package com.gitlab.taucher2003.gitlab_utility_bot.query;

import com.gitlab.taucher2003.gitlab_utility_bot.entity.Issue;
import io.micronaut.core.annotation.Introspected;

@Introspected
public record UpdateIssueMutationWrapper(UpdateIssueDescriptionMutationFieldWrapper updateIssue) {

    @Introspected
    public record UpdateIssueDescriptionMutationFieldWrapper(Issue issue) {}

}
