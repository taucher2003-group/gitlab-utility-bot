package com.gitlab.taucher2003.gitlab_utility_bot.query;

import com.gitlab.taucher2003.gitlab_utility_bot.entity.MergeRequest;
import io.micronaut.core.annotation.Introspected;

@Introspected
public record MergeRequestQueryWrapper(MergeRequest mergeRequest) {
}
