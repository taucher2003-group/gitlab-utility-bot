package com.gitlab.taucher2003.gitlab_utility_bot.query;

import com.gitlab.taucher2003.gitlab_utility_bot.entity.Note;
import io.micronaut.core.annotation.Introspected;

@Introspected
public record CreateDiscussionNoteMutationWrapper(CreateDiscussionNoteMutationFieldWrapper createNote) {

    @Introspected
    public record CreateDiscussionNoteMutationFieldWrapper(Note note) {
    }
}
