package com.gitlab.taucher2003.gitlab_utility_bot.entity;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.gitlab.taucher2003.gitlab_utility_bot.util.GlobalID;
import io.micronaut.core.annotation.Introspected;

@Introspected
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "__typename", defaultImpl = Todoable.GenericTodoable.class)
@JsonSubTypes({
    @JsonSubTypes.Type(value = Issue.class, name = "Issue"),
    @JsonSubTypes.Type(value = MergeRequest.class, name = "MergeRequest")
})
public interface Todoable {

    String webUrl();

    String __typename();

    GlobalID id();

    record GenericTodoable(String webUrl, String __typename, GlobalID id) implements Todoable {}
}
