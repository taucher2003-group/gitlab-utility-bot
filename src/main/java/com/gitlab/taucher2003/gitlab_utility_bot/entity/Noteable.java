package com.gitlab.taucher2003.gitlab_utility_bot.entity;

import com.gitlab.taucher2003.gitlab_utility_bot.util.GlobalID;

@FunctionalInterface
public interface Noteable {
    GlobalID id();
}
