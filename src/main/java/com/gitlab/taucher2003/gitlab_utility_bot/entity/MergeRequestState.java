package com.gitlab.taucher2003.gitlab_utility_bot.entity;

public enum MergeRequestState {
    all,
    closed,
    locked,
    merged,
    opened
}
