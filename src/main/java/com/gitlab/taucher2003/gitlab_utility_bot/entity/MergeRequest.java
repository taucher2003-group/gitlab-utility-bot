package com.gitlab.taucher2003.gitlab_utility_bot.entity;

import com.gitlab.taucher2003.gitlab_utility_bot.util.GlobalID;
import io.micronaut.core.annotation.Introspected;

import java.util.List;

@Introspected
public record MergeRequest(
    GlobalID id,
    String iid,
    List<DiffStats> diffStats,
    long projectId,
    Project project,
    String title,
    String description,
    String reference,
    Connection<Pipeline> pipelines,
    Connection<UserCore> participants,
    Connection<AwardEmoji> awardEmoji,
    MergeRequestState state,
    String webUrl,
    String __typename
) implements Noteable, Referenceable, Todoable {
}
