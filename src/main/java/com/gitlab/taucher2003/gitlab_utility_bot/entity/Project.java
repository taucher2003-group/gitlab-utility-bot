package com.gitlab.taucher2003.gitlab_utility_bot.entity;

import com.gitlab.taucher2003.gitlab_utility_bot.util.GlobalID;
import io.micronaut.core.annotation.Introspected;

@Introspected
public record Project(
    GlobalID id,
    String nameWithNamespace,
    String fullPath,
    String visibility,
    Connection<MergeRequest> mergeRequests,
    Connection<Issue> issues,
    String importStatus,
    String webUrl
) {
}
