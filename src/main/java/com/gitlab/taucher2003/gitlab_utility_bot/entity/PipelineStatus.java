package com.gitlab.taucher2003.gitlab_utility_bot.entity;

public enum PipelineStatus {

    CREATED("Created"),
    WAITING_FOR_RESOURCE("Waiting for Resource"),
    PREPARING("Preparing"),
    PENDING("Pending"),
    RUNNING("Running"),
    FAILED("Failed", true),
    SUCCESS("Passed", true),
    CANCELED("Canceled", true),
    SCHEDULED("Scheduled"),
    MANUAL("Manual"),
    SKIPPED("Skipped", true);

    private final String displayName;
    private final boolean endState;

    PipelineStatus(String displayName) {
        this(displayName, false);
    }

    PipelineStatus(String displayName, boolean endState) {
        this.displayName = displayName;
        this.endState = endState;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean isEndState() {
        return endState;
    }
}
