package com.gitlab.taucher2003.gitlab_utility_bot.entity;

import com.gitlab.taucher2003.gitlab_utility_bot.util.GlobalID;
import io.micronaut.core.annotation.Introspected;

@Introspected
public record Note(
    UserCore author,
    String body,
    Discussion discussion,
    GlobalID id,
    String url
) {
}
