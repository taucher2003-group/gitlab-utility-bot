package com.gitlab.taucher2003.gitlab_utility_bot.entity;

import com.gitlab.taucher2003.gitlab_utility_bot.util.GlobalID;
import io.micronaut.core.annotation.Introspected;

@Introspected
public record Todo(
    GlobalID id,
    UserCore author,
    String action,
    String body,
    Note note,
    Todoable targetEntity
) {
}
