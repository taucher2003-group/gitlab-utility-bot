package com.gitlab.taucher2003.gitlab_utility_bot.entity;

public enum IssueState {
    all,
    closed,
    locked,
    opened
}
