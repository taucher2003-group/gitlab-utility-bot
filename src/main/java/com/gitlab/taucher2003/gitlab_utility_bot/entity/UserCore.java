package com.gitlab.taucher2003.gitlab_utility_bot.entity;

import com.gitlab.taucher2003.gitlab_utility_bot.util.GlobalID;
import io.micronaut.core.annotation.Introspected;

@Introspected
public record UserCore(
    GlobalID id,
    String username,
    Connection<Todo> todos
) {

    public String mention() {
        return "@" + username;
    }
}
