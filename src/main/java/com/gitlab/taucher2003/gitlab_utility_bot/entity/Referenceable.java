package com.gitlab.taucher2003.gitlab_utility_bot.entity;

@FunctionalInterface
public interface Referenceable {
    String reference();
}
