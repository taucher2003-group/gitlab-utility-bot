package com.gitlab.taucher2003.gitlab_utility_bot.entity;

import com.gitlab.taucher2003.gitlab_utility_bot.util.GlobalID;
import io.micronaut.core.annotation.Introspected;

@Introspected
public record Issue(
    GlobalID id,
    String iid,
    long projectId,
    String title,
    String description,
    String reference,
    IssueState state,
    boolean confidential,
    Connection<MergeRequest> relatedMergeRequests,
    String webUrl,
    String __typename
) implements Noteable, Referenceable, Todoable {
}
