package com.gitlab.taucher2003.gitlab_utility_bot.entity;

import io.micronaut.core.annotation.Introspected;

@Introspected
public record DiffStats(
    String path,
    int additions,
    int deletions
) {
}
