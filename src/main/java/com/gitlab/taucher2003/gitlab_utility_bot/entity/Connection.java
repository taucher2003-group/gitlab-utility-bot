package com.gitlab.taucher2003.gitlab_utility_bot.entity;

import io.micronaut.core.annotation.Introspected;

import java.util.List;

@Introspected
public record Connection<T>(List<T> nodes) {
}
