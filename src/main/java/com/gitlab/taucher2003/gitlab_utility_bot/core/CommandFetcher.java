package com.gitlab.taucher2003.gitlab_utility_bot.core;

import com.gitlab.taucher2003.gitlab_utility_bot.command.ReactiveCommand;
import com.gitlab.taucher2003.gitlab_utility_bot.entity.Todo;
import com.gitlab.taucher2003.gitlab_utility_bot.graphql.GraphQLClient;
import com.gitlab.taucher2003.gitlab_utility_bot.query.CurrentUserQueryWrapper;
import io.micronaut.context.annotation.Context;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

@Singleton
@Context
public class CommandFetcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandFetcher.class);
    private static final String ALLOWED_USER_IDS_FLAG = "allowed_userid";
    private static final String ALLOWED_USERNAMES_FLAG = "allowed_username";

    private final GraphQLClient client;
    private final ActionStorage actionStorage;
    private final UserContext userContext;
    private final List<ReactiveCommand> commands;
    private final UnleashFactory.UnleashContainer unleashContainer;
    private final Map<String, ReactiveCommand> invokeCommandMap = new HashMap<>();

    public CommandFetcher(
        GraphQLClient client,
        ActionStorage actionStorage,
        UserContext userContext,
        List<ReactiveCommand> commands,
        UnleashFactory.UnleashContainer unleashContainer
    ) {
        this.client = client;
        this.actionStorage = actionStorage;
        this.userContext = userContext;
        this.commands = commands;
        this.unleashContainer = unleashContainer;
    }

    @PostConstruct
    void init() {
        commands.forEach(command -> {
            if (invokeCommandMap.containsKey(command.commandName().toLowerCase(Locale.ROOT))) {
                LOGGER.warn("Alias '{}' is being overwritten with a command", command.commandName());
            }
            invokeCommandMap.put(command.commandName().toLowerCase(Locale.ROOT), command);
            LOGGER.info("Registered command '{}'", command.commandName());
            command.aliases().forEach(alias -> {
                if (invokeCommandMap.containsKey(alias.toLowerCase(Locale.ROOT))) {
                    LOGGER.warn("Ignoring alias '{}' for '{}' because it already exists as command", alias, command.commandName());
                    return;
                }
                invokeCommandMap.put(alias.toLowerCase(Locale.ROOT), command);
                LOGGER.info("Registered alias '{}' for command '{}'", alias, command.commandName());
            });
        });
    }

    List<Todo> fetchTodos() {
        var todos = client.executeQuery(client.loadQuery("todo/FetchTodos", "todo/TodoFragment", "issue/IssueFragment"), CurrentUserQueryWrapper.class)
            .currentUser()
            .todos()
            .nodes();
        if (!todos.isEmpty()) {
            LOGGER.info("Found {} todos", todos.size());
        }

        return todos;
    }

    void handleTodo(Todo todo) {
        if (todo.targetEntity() == null) {
            LOGGER.info("Found todo with a null targetEntity, marking as done");
            reject(todo);
            return;
        }

        var splitBody = todo.body().split(" ", 3);
        if (!splitBody[0].equals(userContext.getUser().mention())) {
            LOGGER.info("Found todo without ping at beginning, marking as done");
            reject(todo);
            return;
        }

        var commandName = splitBody[1];
        var command = invokeCommandMap.get(commandName);
        if (command == null) {
            LOGGER.warn("Todo for unknown command '{}' found", commandName);
            reject(todo);
            return;
        }

        if ("directly_addressed".equals(todo.action()) || "mentioned".equals(todo.action())) {
            if (todo.note() == null) {
                LOGGER.warn("No note for command '{}' found", commandName);
                return;
            }

            if (
                !unleashContainer.unleash().isEnabled(
                    ALLOWED_USER_IDS_FLAG,
                    UnleashFactory.createContext(unleashContainer.config(), todo.author().id().toString())
                )
                    && !unleashContainer.unleash().isEnabled(
                    ALLOWED_USERNAMES_FLAG,
                    UnleashFactory.createContext(unleashContainer.config(), todo.author().username())
                )
            ) {
                LOGGER.info("Rejecting command '{}' by {} in {}", commandName, todo.author().username(), todo.targetEntity().webUrl());
                reject(todo);
                return;
            }

            LOGGER.info("Executing command '{}' by {} in {}", commandName, todo.author().username(), todo.targetEntity().webUrl());
            var context = new ReactiveCommand.CommandContext(todo, client, actionStorage, extractArguments(commandName, todo.body()));
            var result = command.execute(context);
            result.accept(context);

            if (result != ReactiveCommand.ExecutionResult.STORE_TODO) {
                actionStorage.removeTodo(todo);
            }
        }
    }

    private List<String> extractArguments(String command, String body) {
        var cleanedBody = body
            .replaceFirst(Pattern.quote(userContext.getUser().mention()), "")
            .replaceFirst(Pattern.quote(command), "");
        if (cleanedBody.length() < 2) {
            return Collections.emptyList();
        }

        return Arrays.asList(cleanedBody.substring(2).split(" "));
    }

    private void reject(Todo todo) {
        client.executeQuery(
            client.loadQuery("note/AwardEmojiAdd"),
            Map.of(
                "awardableId", todo.note().id().gid(),
                "name", "x"
            ),
            Void.class
        );
        ReactiveCommand.ExecutionResult.MARK_TODO_AS_DONE.accept(new ReactiveCommand.CommandContext(todo, client, actionStorage));
    }
}
