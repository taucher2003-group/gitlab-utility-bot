package com.gitlab.taucher2003.gitlab_utility_bot.core;

import io.getunleash.repository.FeatureToggleResponse;
import io.micronaut.context.annotation.Context;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.List;

import static com.gitlab.taucher2003.gitlab_utility_bot.core.Scheduler.FF_EXECUTE_FILES_OF_INTEREST_ISSUE_HOUSEKEEPING;
import static com.gitlab.taucher2003.gitlab_utility_bot.core.Scheduler.FF_EXECUTE_FILES_OF_INTEREST_NOTIFICATION;
import static com.gitlab.taucher2003.gitlab_utility_bot.core.Scheduler.FF_EXECUTE_PENDING_COMMANDS;
import static com.gitlab.taucher2003.gitlab_utility_bot.core.Scheduler.FF_EXECUTE_STORED_TODOS;
import static com.gitlab.taucher2003.gitlab_utility_bot.scheduled.ForkMirrorMonitoring.FF_EXECUTE_GITLAB_COMMUNITY_FORK_MIRROR_MONITORING;

@Singleton
@Context
public class UnleashEventLog {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnleashEventLog.class);

    private final UnleashFactory.UnleashContainer unleashContainer;
    private final UnleashHandler unleashHandler;

    private final List<String> monitoredFlags = List.of(
        FF_EXECUTE_PENDING_COMMANDS,
        FF_EXECUTE_STORED_TODOS,
        FF_EXECUTE_FILES_OF_INTEREST_NOTIFICATION,
        FF_EXECUTE_FILES_OF_INTEREST_ISSUE_HOUSEKEEPING,
        FF_EXECUTE_GITLAB_COMMUNITY_FORK_MIRROR_MONITORING
    );

    public UnleashEventLog(UnleashFactory.UnleashContainer unleashContainer, UnleashHandler unleashHandler) {
        this.unleashContainer = unleashContainer;
        this.unleashHandler = unleashHandler;
    }

    @PostConstruct
    void init() {
        unleashHandler.subscribeToToggleFetches(this::togglesFetched);
    }

    public void togglesFetched(FeatureToggleResponse toggleResponse) {
        if (toggleResponse.getStatus().equals(FeatureToggleResponse.Status.CHANGED)) {
            LOGGER.info("Feature Flags have changed. New values are:");
            monitoredFlags.forEach(flag -> LOGGER.info("%s: %s".formatted(flag, unleashContainer.unleash().isEnabled(flag))));
        }
    }
}
