package com.gitlab.taucher2003.gitlab_utility_bot.core;

import io.getunleash.event.UnleashReady;
import io.getunleash.event.UnleashSubscriber;
import io.getunleash.repository.FeatureToggleResponse;
import jakarta.inject.Singleton;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;

@Singleton
public class UnleashHandler implements UnleashSubscriber {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnleashHandler.class);

    private final Collection<Consumer<FeatureToggleResponse>> featureToggleSubscribers = new ArrayList<>();
    private boolean ready;

    @Override
    public void onReady(@NotNull UnleashReady unleashReady) {
        LOGGER.info("Unleash is ready");
        ready = true;
    }

    @Override
    public void togglesFetched(@NotNull FeatureToggleResponse toggleResponse) {
        if (!ready) {
            return;
        }
        featureToggleSubscribers.forEach(consumer -> consumer.accept(toggleResponse));
    }

    public void subscribeToToggleFetches(Consumer<FeatureToggleResponse> consumer) {
        featureToggleSubscribers.add(consumer);
    }

    public void unsubscribeFromToggleFetches(Consumer<FeatureToggleResponse> consumer) {
        featureToggleSubscribers.remove(consumer);
    }
}
