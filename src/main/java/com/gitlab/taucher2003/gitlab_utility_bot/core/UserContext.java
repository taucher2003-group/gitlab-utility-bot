package com.gitlab.taucher2003.gitlab_utility_bot.core;

import com.gitlab.taucher2003.gitlab_utility_bot.entity.UserCore;
import com.gitlab.taucher2003.gitlab_utility_bot.graphql.GraphQLClient;
import com.gitlab.taucher2003.gitlab_utility_bot.graphql.SudoGraphQLClient;
import com.gitlab.taucher2003.gitlab_utility_bot.query.CurrentUserQueryWrapper;
import io.micronaut.context.annotation.Context;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.http.client.exceptions.HttpClientException;
import io.micronaut.retry.annotation.Retryable;
import io.micronaut.scheduling.annotation.Scheduled;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.UnknownHostException;
import java.util.Map;

@Singleton
@Context
public class UserContext {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserContext.class);

    private final GraphQLClient client;
    private final SudoGraphQLClient sudoClient;

    private UserCore user;

    public UserContext(GraphQLClient client, @Nullable SudoGraphQLClient sudoClient) {
        this.client = client;
        this.sudoClient = sudoClient;
    }

    @Scheduled(initialDelay = "1s")
    @Retryable(includes = {HttpClientException.class, UnknownHostException.class}, delay = "10s")
    public void init() {
        user = client.executeQuery(client.loadQuery("CurrentUser"), CurrentUserQueryWrapper.class).currentUser();
        if (user == null) {
            LOGGER.error("Failed to login");
            throw new IllegalArgumentException("Failed to login, possibly a wrong token?");
        }
        LOGGER.info("Logged in with user {}", user.username());

        if (sudoClient != null) {
            var sudoUser = sudoClient.executeQuery(
                    client.loadQuery("CurrentUser"),
                    Map.of(),
                    CurrentUserQueryWrapper.class
                )
                .currentUser();
            LOGGER.info("Logged in sudo-client with user {}", sudoUser.username());
        }
    }

    public UserCore getUser() {
        return user;
    }
}
