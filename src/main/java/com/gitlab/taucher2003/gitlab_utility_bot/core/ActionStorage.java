package com.gitlab.taucher2003.gitlab_utility_bot.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.taucher2003.gitlab_utility_bot.entity.Todo;
import com.gitlab.taucher2003.gitlab_utility_bot.graphql.GraphQLClient;
import com.gitlab.taucher2003.gitlab_utility_bot.query.UpdateIssueMutationWrapper;
import com.gitlab.taucher2003.gitlab_utility_bot.util.GlobalID;
import com.gitlab.taucher2003.gitlab_utility_bot.util.Helpers;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Requires;
import io.micronaut.context.annotation.Value;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.client.exceptions.HttpClientException;
import io.micronaut.retry.annotation.Retryable;
import io.micronaut.scheduling.annotation.Scheduled;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
@Context
@Requires(property = "GITLAB_DATA_STORE_PROJECT")
@Requires(property = "GITLAB_DATA_STORE_ISSUE")
public class ActionStorage {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActionStorage.class);

    private final GraphQLClient graphQLClient;
    private final ObjectMapper objectMapper;
    private final Helpers helpers;
    private final GlobalID issueId;
    private final String projectPath;

    private String issueIid;

    public ActionStorage(
        GraphQLClient graphQLClient,
        ObjectMapper objectMapper,
        Helpers helpers,
        @Value("${gitlab.data.store.issue}") String issueId,
        @Value("${gitlab.data.store.project}") String projectPath
    ) {
        this.graphQLClient = graphQLClient;
        this.objectMapper = objectMapper;
        this.helpers = helpers;
        this.issueId = GlobalID.of(issueId);
        this.projectPath = projectPath;
    }

    @Scheduled(initialDelay = "1s")
    @Retryable(includes = {HttpClientException.class, UnknownHostException.class}, delay = "10s")
    void init() {
        var issue = helpers.getIssue(issueId);
        this.issueIid = issue.iid();
    }

    public void storeTodo(Todo todo) {
        var dataStore = getDataStore();
        if (!dataStore.todos().contains(todo.id())) {
            LOGGER.info("Storing todo {} in ActionStorage", todo.id());
            dataStore.todos().add(todo.id());
            saveDataStore(dataStore);
        }
    }

    public void removeTodo(Todo todo) {
        var dataStore = getDataStore();
        if (dataStore.todos().contains(todo.id())) {
            LOGGER.info("Removing todo {} from ActionStorage", todo.id());
            dataStore.todos().remove(todo.id());
            saveDataStore(dataStore);
        }
    }

    public DataStore getDataStore() {
        var issue = helpers.getIssue(issueId);
        if ("".equals(issue.description())) {
            return new DataStore(new HashSet<>());
        }
        try {
            var store = objectMapper.readValue(issue.description(), DataStore.class);
            if (store.todos() == null) {
                return new DataStore(new HashSet<>());
            }
            return store;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private void saveDataStore(DataStore dataStore) {
        try {
            var query = graphQLClient.loadQuery("issue/UpdateIssueDescription", "issue/IssueFragment");
            var variables = Map.of(
                "projectPath", projectPath,
                "issueIid", issueIid,
                "description", objectMapper.writeValueAsString(dataStore.toWriteable())
            );
            LOGGER.info("Saving DataStore to issue");
            var response = graphQLClient.execute(query, variables, UpdateIssueMutationWrapper.class);
            if (response.errors() != null && !response.errors().isEmpty()) {
                LOGGER.error("Encountered errors while updating issue: {}", response.errors());
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Introspected
    record DataStore(Set<GlobalID> todos) {

        WriteableDataStore toWriteable() {
            return new WriteableDataStore(todos.stream().map(GlobalID::gid).collect(Collectors.toSet()));
        }
    }

    @Introspected
    record WriteableDataStore(Set<String> todos) {}
}
