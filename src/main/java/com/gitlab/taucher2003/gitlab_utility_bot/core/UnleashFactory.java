package com.gitlab.taucher2003.gitlab_utility_bot.core;

import io.getunleash.DefaultUnleash;
import io.getunleash.FakeUnleash;
import io.getunleash.Unleash;
import io.getunleash.UnleashContext;
import io.getunleash.util.UnleashConfig;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Requires;
import io.micronaut.context.annotation.Value;
import jakarta.inject.Singleton;

@Factory
public class UnleashFactory {

    @Bean
    @Singleton
    @Context
    @Requires(notEnv = "test")
    public UnleashConfig unleashConfig(
        @Value("${unleash.instance}") String instance,
        @Value("${unleash.host}") String host,
        @Value("${unleash.environment}") String environment,
        UnleashHandler unleashHandler
    ) {
        return UnleashConfig
            .builder()
            .appName("gl-utility-bot")
            .environment(environment)
            .instanceId(instance)
            .unleashAPI(host)
            .disableMetrics()
            .fetchTogglesInterval(60)
            .subscriber(unleashHandler)
            .build();
    }

    @Bean
    @Singleton
    @Context
    @Requires(notEnv = "test")
    public UnleashContainer unleash(UnleashConfig config) {
        return new UnleashContainer(new DefaultUnleash(config), config);
    }

    @Singleton
    @Context
    @Requires(env = "test")
    public UnleashContainer fakeUnleash() {
        return new UnleashContainer(new FakeUnleash(), null);
    }

    public static UnleashContext createContext(UnleashConfig config) {
        return UnleashContext.builder().build().applyStaticFields(config);
    }

    public static UnleashContext createContext(UnleashConfig config, String userId) {
        return UnleashContext.builder().userId(userId).build().applyStaticFields(config);
    }

    public record UnleashContainer(Unleash unleash, UnleashConfig config) {}
}
