package com.gitlab.taucher2003.gitlab_utility_bot.core;

import com.gitlab.taucher2003.gitlab_utility_bot.util.Helpers;
import io.micronaut.context.annotation.Context;
import io.micronaut.scheduling.annotation.Scheduled;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
@Context
public class Scheduler {

    private static final Logger LOGGER = LoggerFactory.getLogger(Scheduler.class);

    public static final String FF_EXECUTE_PENDING_COMMANDS = "execute_pending_commands";
    public static final String FF_EXECUTE_STORED_TODOS = "execute_stored_todos";
    public static final String FF_EXECUTE_FILES_OF_INTEREST_NOTIFICATION = "execute_files_of_interest_notification";
    public static final String FF_EXECUTE_FILES_OF_INTEREST_ISSUE_HOUSEKEEPING = "execute_files_of_interest_issue_housekeeping";

    private final UserContext userContext;
    private final CommandFetcher commandFetcher;
    private final ActionStorage actionStorage;
    private final NotificationService notificationService;
    private final Helpers helpers;
    private final UnleashFactory.UnleashContainer unleashContainer;

    public Scheduler(
        UserContext userContext,
        CommandFetcher commandFetcher,
        ActionStorage actionStorage,
        NotificationService notificationService,
        Helpers helpers,
        UnleashFactory.UnleashContainer unleashContainer
    ) {
        this.userContext = userContext;
        this.commandFetcher = commandFetcher;
        this.actionStorage = actionStorage;
        this.notificationService = notificationService;
        this.helpers = helpers;
        this.unleashContainer = unleashContainer;
    }

    @Scheduled(fixedDelay = "10s", initialDelay = "5s")
    void executePendingCommands() {
        if (userContext.getUser() == null) {
            LOGGER.warn("UserContext not ready, skipping this run");
            return;
        }

        if (!unleashContainer.unleash().isEnabled(FF_EXECUTE_PENDING_COMMANDS)) {
            return;
        }

        var todos = commandFetcher.fetchTodos();
        todos.forEach(commandFetcher::handleTodo);
    }

    @Scheduled(fixedDelay = "10s", initialDelay = "10s")
    void executeStoredTodos() {
        if (userContext.getUser() == null) {
            LOGGER.warn("UserContext not ready, skipping this run");
            return;
        }

        if (!unleashContainer.unleash().isEnabled(FF_EXECUTE_STORED_TODOS)) {
            return;
        }

        var store = actionStorage.getDataStore();
        if (store.todos() == null) {
            return;
        }

        var todos = store.todos()
            .stream()
            .map(helpers::getTodo)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .collect(Collectors.toSet());

        todos.forEach(commandFetcher::handleTodo);
    }

    @Scheduled(fixedDelay = "60s", initialDelay = "15s")
    void executeNotificationService() {
        if (unleashContainer.unleash().isEnabled(FF_EXECUTE_FILES_OF_INTEREST_NOTIFICATION)) {
            notificationService.notifyFilesOfInterest();
        }
        if (unleashContainer.unleash().isEnabled(FF_EXECUTE_FILES_OF_INTEREST_ISSUE_HOUSEKEEPING)) {
            notificationService.updateReportingIssues();
        }
    }
}
