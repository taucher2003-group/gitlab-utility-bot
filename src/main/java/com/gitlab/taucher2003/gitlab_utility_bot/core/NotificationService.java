package com.gitlab.taucher2003.gitlab_utility_bot.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.taucher2003.gitlab_utility_bot.entity.DiffStats;
import com.gitlab.taucher2003.gitlab_utility_bot.entity.IssueState;
import com.gitlab.taucher2003.gitlab_utility_bot.entity.MergeRequest;
import com.gitlab.taucher2003.gitlab_utility_bot.entity.MergeRequestState;
import com.gitlab.taucher2003.gitlab_utility_bot.entity.Project;
import com.gitlab.taucher2003.gitlab_utility_bot.graphql.GraphQLClient;
import com.gitlab.taucher2003.gitlab_utility_bot.graphql.SudoGraphQLClient;
import com.gitlab.taucher2003.gitlab_utility_bot.query.MergeRequestQueryWrapper;
import com.gitlab.taucher2003.gitlab_utility_bot.query.ProjectQueryWrapper;
import com.gitlab.taucher2003.gitlab_utility_bot.util.CommentHelpers;
import com.gitlab.taucher2003.gitlab_utility_bot.util.GlobalID;
import com.gitlab.taucher2003.gitlab_utility_bot.util.Helpers;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Requires;
import io.micronaut.context.annotation.Value;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Singleton
@Context
@Requires(property = "GITLAB_DATA_CONFIG_ISSUE")
public class NotificationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);

    private final GraphQLClient graphQLClient;
    private final SudoGraphQLClient sudoGraphQLClient;
    private final ObjectMapper objectMapper;
    private final Helpers helpers;
    private final CommentHelpers commentHelpers;
    private final UserContext userContext;
    private final GlobalID issueId;

    public NotificationService(
        GraphQLClient graphQLClient,
        SudoGraphQLClient sudoGraphQLClient,
        ObjectMapper objectMapper,
        Helpers helpers,
        CommentHelpers commentHelpers,
        UserContext userContext,
        @Value("${gitlab.data.config.issue}") String issueId
    ) {
        this.graphQLClient = graphQLClient;
        this.sudoGraphQLClient = sudoGraphQLClient;
        this.objectMapper = objectMapper;
        this.helpers = helpers;
        this.commentHelpers = commentHelpers;
        this.userContext = userContext;
        this.issueId = GlobalID.of(issueId);
    }

    public NotificationConfig getNotificationConfig() {
        var issue = helpers.getIssue(issueId);
        if ("".equals(issue.description())) {
            return new NotificationConfig(Collections.emptyMap(), Collections.emptyList(), new ReportingConfig(null, Collections.emptyList()));
        }
        try {
            var store = objectMapper.readValue(issue.description(), NotificationConfig.class);
            if (store == null) {
                return new NotificationConfig(Collections.emptyMap(), Collections.emptyList(), new ReportingConfig(null, Collections.emptyList()));
            }
            return store;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    void notifyFilesOfInterest() {
        var config = getNotificationConfig();
        if (config == null) {
            LOGGER.warn("No configuration found");
            return;
        }

        var projectConfigurations = config.files_of_interest();
        if (projectConfigurations == null) {
            LOGGER.warn("No project configuration found");
            return;
        }

        var notificationUsers = config.notification_users();

        var query = graphQLClient.loadQuery("merge_request/MergeRequestDiffFiles");
        projectConfigurations.forEach((projectName, listOfFiles) -> {
            LOGGER.debug("Searching for MRs in {}", projectName);
            Project project;
            try {
                project = graphQLClient.executeQuery(query, Map.of("project", projectName), ProjectQueryWrapper.class).project();
            } catch (HttpClientResponseException e) {
                LOGGER.warn("Failed to search MRs in {}", projectName);
                LOGGER.warn("Correlation ID is {}", e.getResponse().header("X-Request-Id"));
                if (e.getStatus().getCode() < HttpStatus.INTERNAL_SERVER_ERROR.getCode()) {
                    throw e;
                }
                return;
            }
            project.mergeRequests().nodes().forEach(mergeRequest -> {
                if (mergeRequest.state().equals(MergeRequestState.locked)) {
                    return;
                }

                var changedFilesOfInterest = mergeRequest.diffStats()
                    .stream()
                    .map(DiffStats::path)
                    .filter(getInterestedFilePredicate(listOfFiles))
                    .toList();

                if (changedFilesOfInterest.isEmpty()) {
                    return;
                }

                var botHasAlreadyNotified = mergeRequest.awardEmoji()
                    .nodes()
                    .stream()
                    .filter(emoji -> emoji.user().id().equals(userContext.getUser().id()))
                    .anyMatch(emoji -> "bookmark_tabs".equals(emoji.name()));
                if (botHasAlreadyNotified) {
                    return;
                }

                LOGGER.info("Notifying on {}", mergeRequest.webUrl());

                var awardEmojiQuery = graphQLClient.loadQuery("note/AwardEmojiAdd");

                sudoGraphQLClient.executeQuery(
                    awardEmojiQuery,
                    Map.of(
                        "awardableId", mergeRequest.id().gid(),
                        "name", "robot"
                    ),
                    Void.class
                );
                graphQLClient.executeQuery(
                    awardEmojiQuery,
                    Map.of(
                        "awardableId", mergeRequest.id().gid(),
                        "name", "bookmark_tabs"
                    ),
                    Void.class
                );

                helpers.createIssue(
                    config.reporting().project(),
                    "%s | %s".formatted(mergeRequest.project().fullPath(), mergeRequest.title()),
                    buildDescriptionForReportingIssue(mergeRequest, changedFilesOfInterest, notificationUsers),
                    config.reporting().labels(),
                    !"public".equals(mergeRequest.project().visibility())
                );
            });
        });
    }

    private Predicate<String> getInterestedFilePredicate(Collection<String> interestedFilesConfig) {
        var pathPredicates = interestedFilesConfig.stream().map(entry -> {
            Predicate<String> predicate;
            if (entry.startsWith("contains:")) {
                var filename = entry.replaceFirst("contains:", "");
                predicate = path -> path.contains(filename);
            } else if (entry.startsWith("regex:")) {
                var pattern = entry.replaceFirst("regex:", "");
                predicate = path -> path.matches(pattern);
            } else if (entry.startsWith("start:")) {
                var prefix = entry.replaceFirst("start:", "");
                predicate = path -> path.startsWith(prefix);
            } else {
                predicate = path -> path.equals(entry);
            }
            return predicate;
        }).toList();

        return path -> pathPredicates.stream().anyMatch(predicate -> predicate.test(path));
    }

    void updateReportingIssues() {
        var config = getNotificationConfig();
        if (config == null) {
            LOGGER.warn("No configuration found");
            return;
        }

        updateReportingIssues(config, IssueState.opened);
        updateReportingIssues(config, IssueState.closed);
    }

    private void updateReportingIssues(NotificationConfig config, IssueState state) {
        var project = graphQLClient.executeQuery(
            graphQLClient.loadQuery("issue/FetchIssues", "issue/IssueFragment"),
            Map.of(
                "projectPath", config.reporting().project(),
                "labels", config.reporting().labels(),
                "state", state.name()
            ),
            ProjectQueryWrapper.class
        ).project();

        var mergeRequestQuery = graphQLClient.loadQuery("merge_request/FetchMergeRequest");
        var updateIssueTitleMutation = graphQLClient.loadQuery("issue/UpdateIssueTitle", "issue/IssueFragment");
        var updateIssueConfidentialMutation = graphQLClient.loadQuery("issue/UpdateIssueConfidential", "issue/IssueFragment");

        var gidRegex = Pattern.compile("<!-- (?<gid>.+?) -->");
        project.issues().nodes().forEach(issue -> {
            var matcher = gidRegex.matcher(issue.description());
            if (!matcher.find()) {
                return;
            }

            var mergeRequestId = GlobalID.of(matcher.group("gid"));
            var mergeRequest = graphQLClient.executeQuery(
                mergeRequestQuery,
                Map.of("mergeRequestId", mergeRequestId.gid()),
                MergeRequestQueryWrapper.class
            ).mergeRequest();

            if (mergeRequest == null) { // MR doesn't exist anymore
                commentHelpers.comment(issue, "/close");

                // non-existing MRs should be hidden from public lists
                graphQLClient.executeQuery(
                    updateIssueConfidentialMutation,
                    Map.of(
                        "projectPath", config.reporting().project(),
                        "issueIid", issue.iid(),
                        "confidential", true
                    ),
                    Void.class
                );
                return;
            }

            var desiredTitle = "%s | %s".formatted(mergeRequest.project().fullPath(), mergeRequest.title());
            if (!issue.title().equals(desiredTitle)) {
                graphQLClient.executeQuery(
                    updateIssueTitleMutation,
                    Map.of(
                        "projectPath", config.reporting().project(),
                        "issueIid", issue.iid(),
                        "title", desiredTitle
                    ),
                    Void.class
                );
            }

            var desiredConfidential = issue.confidential() || !"public".equals(mergeRequest.project().visibility());
            if (issue.confidential() != desiredConfidential) {
                graphQLClient.executeQuery(
                    updateIssueConfidentialMutation,
                    Map.of(
                        "projectPath", config.reporting().project(),
                        "issueIid", issue.iid(),
                        "confidential", true
                    ),
                    Void.class
                );
            }

            if (
                !mergeRequest.state().equals(MergeRequestState.opened)
                    && issue.state().equals(IssueState.opened)
            ) {
                commentHelpers.comment(issue, "/close");
            }
            if (
                mergeRequest.state().equals(MergeRequestState.opened)
                    && issue.state().equals(IssueState.closed)
            ) {
                commentHelpers.comment(issue, "/reopen");
            }
        });
    }

    private String buildDescriptionForReportingIssue(
        MergeRequest mergeRequest,
        Collection<String> changedFilesOfInterest,
        Collection<String> notificationUsers
    ) {
        return """
            <!-- %s -->

            Hey %s :wave:

            Some files of your interest have been changed.

            ## Merge Request

            %s+s

            ## Changed files of interest

            %s

            """
            .formatted(
                mergeRequest.id().gid(),
                notificationUsers.stream().map(username -> "@" + username).collect(Collectors.joining(", ")),
                mergeRequest.webUrl(),
                changedFilesOfInterest.stream().map(path -> "- " + path).collect(Collectors.joining("\n"))
            );
    }

    @Introspected
    record NotificationConfig(Map<String, List<String>> files_of_interest, List<String> notification_users,
                              ReportingConfig reporting) {}

    @Introspected
    record ReportingConfig(String project, List<String> labels) {}
}
