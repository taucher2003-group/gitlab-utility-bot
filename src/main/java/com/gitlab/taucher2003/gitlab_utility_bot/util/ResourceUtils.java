package com.gitlab.taucher2003.gitlab_utility_bot.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

public final class ResourceUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceUtils.class);

    private ResourceUtils() {
    }

    private static InputStream getStreamFromResource(String fileName) {

        var classLoader = ResourceUtils.class.getClassLoader();
        var resource = classLoader.getResourceAsStream(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file not found! " + fileName);
        }

        return resource;
    }

    public static String readStream(InputStream stream) {
        try {
            return new String(stream.readAllBytes());
        } catch (IOException e) {
            LOGGER.error("Failed to read resource file", e);
            throw new RuntimeException(e);
        }
    }

    public static String readResourceByName(String fileName) {
        return readStream(getStreamFromResource(fileName));
    }
}
