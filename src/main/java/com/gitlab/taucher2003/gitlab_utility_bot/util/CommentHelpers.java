package com.gitlab.taucher2003.gitlab_utility_bot.util;

import com.gitlab.taucher2003.gitlab_utility_bot.core.UserContext;
import com.gitlab.taucher2003.gitlab_utility_bot.entity.Note;
import com.gitlab.taucher2003.gitlab_utility_bot.entity.Noteable;
import com.gitlab.taucher2003.gitlab_utility_bot.entity.Todo;
import com.gitlab.taucher2003.gitlab_utility_bot.graphql.GraphQLClient;
import com.gitlab.taucher2003.gitlab_utility_bot.graphql.SudoGraphQLClient;
import com.gitlab.taucher2003.gitlab_utility_bot.query.CreateDiscussionNoteMutationWrapper;
import io.micronaut.core.annotation.Nullable;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

@Singleton
public class CommentHelpers {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommentHelpers.class);

    private final UserContext userContext;
    private final GraphQLClient graphQLClient;
    private final SudoGraphQLClient sudoGraphQLClient;

    public CommentHelpers(UserContext userContext, GraphQLClient graphQLClient, @Nullable SudoGraphQLClient sudoGraphQLClient) {
        this.userContext = userContext;
        this.graphQLClient = graphQLClient;
        this.sudoGraphQLClient = sudoGraphQLClient;
    }

    public void comment(Noteable noteable, String message) {
        comment(graphQLClient, message, noteable.id().gid(), null);
    }

    public void reply(Todo todo, String message) {
        reply(todo.note(), todo.targetEntity().id(), message);
    }

    public void reply(Note note, GlobalID noteableId, String message) {
        reply(graphQLClient, note, noteableId, message);
    }

    public void sudoReply(Todo todo, String message) {
        sudoReply(todo.note(), todo.targetEntity().id(), message);
    }

    public void sudoReply(Note note, GlobalID noteableId, String message) {
        if (sudoGraphQLClient != null) {
            var suffixedMessage = """
                %s

                :robot: This message was generated via `%s`""".formatted(message, userContext.getUser().mention());

            reply(sudoGraphQLClient, note, noteableId, suffixedMessage);
            return;
        }
        reply(graphQLClient, note, noteableId, message);
    }

    private void reply(GraphQLClient client, Note note, GlobalID noteableId, String message) {
        comment(client, message, noteableId.gid(), note.discussion().replyId().gid());
    }

    private void comment(GraphQLClient client, String message, String noteableId, String discussionId) {
        var variables = new HashMap<String, Object>();
        variables.put("body", message);
        variables.put("noteableId", noteableId);
        variables.put("discussionId", discussionId);

        var createdNoteResponse = client.execute(
            client.loadQuery("CreateDiscussionNote"),
            variables,
            CreateDiscussionNoteMutationWrapper.class
        );

        if (createdNoteResponse.errors() != null && !createdNoteResponse.errors().isEmpty()) {
            LOGGER.error("Encountered errors while creating note: {}", createdNoteResponse.errors());
            return;
        }

        if (createdNoteResponse.data().createNote().note() == null) {
            return;
        }

        LOGGER.info("Created note at {}", createdNoteResponse.data().createNote().note().url());
    }
}
