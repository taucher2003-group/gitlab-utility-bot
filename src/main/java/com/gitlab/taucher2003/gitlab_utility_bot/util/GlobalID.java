package com.gitlab.taucher2003.gitlab_utility_bot.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.micronaut.core.annotation.Introspected;

import java.util.regex.Pattern;

@Introspected
public record GlobalID(String type, String id) {

    public static final Pattern GID_PATTERN = Pattern.compile("gid://gitlab/(?<type>[a-zA-Z:]+)/(?<id>\\w+)");

    @JsonCreator
    public static GlobalID of(String gid) {
        var matcher = GID_PATTERN.matcher(gid);
        if (matcher.matches()) {
            return new GlobalID(matcher.group("type"), matcher.group("id"));
        }
        throw new IllegalArgumentException("%s can't be parsed into a GlobalID".formatted(gid));
    }

    public String gid() {
        return toString();
    }

    @Override
    public String toString() {
        return "gid://gitlab/%s/%s".formatted(type, id);
    }
}
