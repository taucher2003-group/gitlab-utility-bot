package com.gitlab.taucher2003.gitlab_utility_bot.util;

import com.gitlab.taucher2003.gitlab_utility_bot.entity.Issue;
import com.gitlab.taucher2003.gitlab_utility_bot.entity.Pipeline;
import com.gitlab.taucher2003.gitlab_utility_bot.entity.Todo;
import com.gitlab.taucher2003.gitlab_utility_bot.graphql.GraphQLClient;
import com.gitlab.taucher2003.gitlab_utility_bot.query.CreateIssueMutationWrapper;
import com.gitlab.taucher2003.gitlab_utility_bot.query.IssueQueryWrapper;
import com.gitlab.taucher2003.gitlab_utility_bot.query.MergeRequestQueryWrapper;
import com.gitlab.taucher2003.gitlab_utility_bot.query.TodoQueryWrapper;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Singleton
public class Helpers {

    private static final Logger LOGGER = LoggerFactory.getLogger(Helpers.class);

    private final GraphQLClient graphQLClient;

    public Helpers(GraphQLClient graphQLClient) {
        this.graphQLClient = graphQLClient;
    }

    public Optional<Pipeline> getMergeRequestPipeline(GlobalID mergeRequestId) {
        var pipelines = graphQLClient.executeQuery(
                graphQLClient.loadQuery("merge_request/MergeRequestPipeline", "pipeline/PipelineFragment"),
                Map.of("mr", mergeRequestId.gid()),
                MergeRequestQueryWrapper.class
            )
            .mergeRequest()
            .pipelines()
            .nodes();

        if (pipelines.isEmpty()) {
            return Optional.empty();
        }

        return Optional.ofNullable(pipelines.get(0));
    }

    public Optional<Todo> getTodo(GlobalID id) {
        var query = graphQLClient.loadQuery("todo/FetchTodo", "todo/TodoFragment", "issue/IssueFragment");
        var todoWrapper = graphQLClient.executeQuery(query, Map.of("todoId", id.gid()), TodoQueryWrapper.class);

        return Optional.ofNullable(todoWrapper.todo());
    }

    public Issue getIssue(GlobalID issueId) {
        var query = graphQLClient.loadQuery("issue/FetchIssue", "issue/IssueFragment");
        return graphQLClient.executeQuery(query, Map.of("issueId", issueId.gid()), IssueQueryWrapper.class).issue();
    }

    public void createIssue(String projectPath, String title, String description, List<String> labels, boolean confidential) {
        var variables = new HashMap<String, Object>();
        variables.put("projectPath", projectPath);
        variables.put("title", title);
        variables.put("description", description);
        variables.put("labels", labels);
        variables.put("confidential", confidential);

        var createdIssueResponse = graphQLClient.execute(
            graphQLClient.loadQuery("issue/CreateIssue", "issue/IssueFragment"),
            variables,
            CreateIssueMutationWrapper.class
        );

        if (createdIssueResponse.errors() != null && !createdIssueResponse.errors().isEmpty()) {
            LOGGER.error("Encountered errors while creating issue: {}", createdIssueResponse.errors());
            return;
        }

        LOGGER.info("Created issue at {}", createdIssueResponse.data().createIssue().issue().webUrl());
    }
}
