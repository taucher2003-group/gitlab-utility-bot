package com.gitlab.taucher2003.gitlab_utility_bot.scheduled;

import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Requires;
import io.micronaut.scheduling.annotation.Scheduled;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
@Context
@Requires(property = "loki.heartbeat")
public class LokiHeartbeat {
    private static final Logger LOGGER = LoggerFactory.getLogger(LokiHeartbeat.class);

    private long heartbeatCount;

    @Scheduled(fixedRate = "10m")
    void run() {
        LOGGER.info("Loki heartbeat {}", ++heartbeatCount);
    }
}
