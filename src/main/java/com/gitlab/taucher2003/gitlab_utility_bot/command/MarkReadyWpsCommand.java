package com.gitlab.taucher2003.gitlab_utility_bot.command;

import com.gitlab.taucher2003.gitlab_utility_bot.entity.MergeRequest;
import com.gitlab.taucher2003.gitlab_utility_bot.entity.PipelineStatus;
import com.gitlab.taucher2003.gitlab_utility_bot.util.CommentHelpers;
import com.gitlab.taucher2003.gitlab_utility_bot.util.Helpers;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Requires;
import jakarta.inject.Singleton;

import java.util.List;
import java.util.stream.Collectors;

@Singleton
@Context
@Requires(property = "ENABLE_GITLAB_ORG_FEATURES")
public class MarkReadyWpsCommand implements ReactiveCommand {
    private final CommentHelpers commentHelpers;
    private final Helpers helpers;

    public MarkReadyWpsCommand(CommentHelpers commentHelpers, Helpers helpers) {
        this.commentHelpers = commentHelpers;
        this.helpers = helpers;
    }

    @Override
    public ExecutionResult execute(CommandContext context) {
        if (!(context.todo().targetEntity() instanceof MergeRequest)) {
            commentHelpers.reply(
                context.todo(),
                "%s this command is not supported on %s".formatted(context.todo().author().mention(), context.todo().targetEntity().__typename())
            );
            return ExecutionResult.DO_NOTHING;
        }

        var pipelineOpt = helpers.getMergeRequestPipeline(context.todo().targetEntity().id());

        if (pipelineOpt.isEmpty()) {
            commentHelpers.reply(
                context.todo(),
                "This Merge Request does not have a pipeline."
            );
            return ExecutionResult.DO_NOTHING;
        }

        var pipeline = pipelineOpt.get();

        if (!pipeline.status().isEndState()) {
            return ExecutionResult.STORE_TODO;
        }

        if (pipeline.status().equals(PipelineStatus.SUCCESS)) {
            var mentions = context.arguments().stream().map(arg -> "@" + arg).collect(Collectors.joining(" "));
            commentHelpers.sudoReply(context.todo(), "@gitlab-bot ready " + mentions);
            return ExecutionResult.MARK_TODO_AS_DONE;
        }

        commentHelpers.reply(
            context.todo(),
            "%s The pipeline was expected to end with `%s` but ended with `%s`".formatted(
                context.todo().author().mention(),
                PipelineStatus.SUCCESS.getDisplayName(),
                pipeline.status().getDisplayName()
            )
        );

        return ExecutionResult.DO_NOTHING;
    }

    @Override
    public String commandName() {
        return "markReadyWps";
    }

    @Override
    public List<String> aliases() {
        return List.of("rwps");
    }
}
