package com.gitlab.taucher2003.gitlab_utility_bot.command;

import com.gitlab.taucher2003.gitlab_utility_bot.core.ActionStorage;
import com.gitlab.taucher2003.gitlab_utility_bot.entity.Todo;
import com.gitlab.taucher2003.gitlab_utility_bot.graphql.GraphQLClient;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public interface ReactiveCommand {
    ExecutionResult execute(CommandContext context);

    String commandName();

    default List<String> aliases() {
        return Collections.emptyList();
    }

    record CommandContext(Todo todo, GraphQLClient graphQLClient, ActionStorage actionStorage, List<String> arguments) {
        public CommandContext(Todo todo, GraphQLClient graphQLClient, ActionStorage actionStorage) {
            this(todo, graphQLClient, actionStorage, Collections.emptyList());
        }
    }

    enum ExecutionResult implements Consumer<CommandContext> {
        DO_NOTHING(ignored -> {
            // used if todo-state should not be toggled
        }),
        RESTORE_TODO(context -> {
            var client = context.graphQLClient();
            client.execute(
                client.loadQuery("todo/RestoreTodo", "todo/TodoFragment", "issue/IssueFragment"),
                Map.of("todo", context.todo().id().gid()),
                Void.class
            );
        }),
        MARK_TODO_AS_DONE(context -> {
            var client = context.graphQLClient();
            client.execute(
                client.loadQuery("todo/MarkTodoAsDone", "todo/TodoFragment", "issue/IssueFragment"),
                Map.of("todo", context.todo().id().gid()),
                Void.class
            );
        }),
        STORE_TODO(context -> {
            context.actionStorage().storeTodo(context.todo());
            MARK_TODO_AS_DONE.accept(context);
        });

        private final Consumer<CommandContext> finalizer;

        ExecutionResult(Consumer<CommandContext> finalizer) {
            this.finalizer = finalizer;
        }

        @Override
        public void accept(CommandContext commandContext) {
            finalizer.accept(commandContext);
        }
    }
}
