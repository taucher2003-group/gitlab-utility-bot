package com.gitlab.taucher2003.gitlab_utility_bot.command;

import com.gitlab.taucher2003.gitlab_utility_bot.util.CommentHelpers;
import io.micronaut.context.annotation.Context;
import jakarta.inject.Singleton;

@Singleton
@Context
public class PingCommand implements ReactiveCommand {

    private final CommentHelpers commentHelpers;

    public PingCommand(CommentHelpers commentHelpers) {
        this.commentHelpers = commentHelpers;
    }

    @Override
    public ExecutionResult execute(CommandContext context) {
        commentHelpers.reply(context.todo(), "Pong! :ping_pong:");
        return ExecutionResult.DO_NOTHING;
    }

    @Override
    public String commandName() {
        return "ping";
    }
}
