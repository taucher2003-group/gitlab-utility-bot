plugins {
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("io.micronaut.application") version "3.3.2"
}

version = "0.1"
group = "com.gitlab.taucher2003"

repositories {
    mavenCentral()
    maven(url = "https://gitlab.com/api/v4/groups/12234336/-/packages/maven")
}

dependencies {
    annotationProcessor("io.micronaut:micronaut-http-validation")
    annotationProcessor("io.micronaut:micronaut-inject-java")
    implementation("io.micronaut:micronaut-http-client")
    implementation("io.micronaut:micronaut-jackson-databind")
    implementation("io.micronaut:micronaut-runtime")
    implementation("io.micronaut:micronaut-inject")
    implementation("io.micronaut.reactor:micronaut-reactor")
    implementation("jakarta.annotation:jakarta.annotation-api")
    runtimeOnly("ch.qos.logback:logback-classic")
    implementation("io.micronaut:micronaut-validation")

    implementation("io.getunleash:unleash-client-java:5.1.0")
    implementation("com.gitlab.taucher2003.t2003-utils:log:1.1-alpha.20")
    implementation("com.github.loki4j:loki-logback-appender:1.4.2")
    implementation("org.codehaus.janino:janino:3.1.8")
}

application {
    mainClass.set("com.gitlab.taucher2003.gitlab_utility_bot.Application")
}
java {
    sourceCompatibility = JavaVersion.VERSION_18
    targetCompatibility = JavaVersion.VERSION_18
}

micronaut {
    runtime("netty")
    testRuntime("junit5")
    processing {
        incremental(true)
        annotations("com.gitlab.taucher2003.*")
    }
}
